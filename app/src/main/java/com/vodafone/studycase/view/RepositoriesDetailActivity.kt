package com.vodafone.studycase.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.vodafone.studycase.databinding.ActivityRepositoriesDetailBinding
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.viewmodel.RepositoriesDetailViewModel
import com.vodafone.studycase.viewmodel.RepositoriesViewModel

class RepositoriesDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityRepositoriesDetailBinding
    var repositories: Repositories? = null
    var viewModel: RepositoriesDetailViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepositoriesDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        repositories = intent.extras!!.getSerializable("repositories") as Repositories?

        if (repositories != null) {
            Glide.with(this).load(repositories?.owner?.avatarUrl).dontAnimate().into(binding.detailImg)
            binding.repoName.text = repositories?.fullName
            binding.userName.text = repositories?.name
            binding.progres.visibility = View.VISIBLE
            viewModel = ViewModelProvider(this).get(RepositoriesDetailViewModel::class.java)
            viewModel!!.refreshData(this, repositories?.owner?.login.toString(),repositories?.name.toString());


            observeliveData()

            binding.view.setOnClickListener(View.OnClickListener {
                val intent = Intent(this, UserDetailActivity::class.java)
                intent.putExtra("login", repositories?.owner?.login)
                startActivity(intent)
            })
        }
    }

    fun observeliveData() {
        viewModel!!.data.observe(this, androidx.lifecycle.Observer { data ->
            data.let {
                if (data != null) {
                    binding.forkCount.text=data.forksCount.toString()
                    binding.language.text=data.language
                    binding.defaultBranch.text=data.defaultBranch

                }
            }
        })

        viewModel!!.dataError.observe(this, androidx.lifecycle.Observer { error ->
            error?.let {
                if (it) {
                    Toast.makeText(this, "Bilinmedik bir hata oluştu", Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel!!.dataLoading.observe(this, androidx.lifecycle.Observer { loading ->
            loading?.let {
                if (it) {
                    binding.progres.visibility = View.VISIBLE
                } else {
                    binding.progres.visibility = View.GONE
                }
            }
        })
    }
}