package com.vodafone.studycase.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.vodafone.studycase.adapter.RepositoriesAdapter
import com.vodafone.studycase.databinding.ActivityUserDetailBinding
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.viewmodel.UserDetailViewModel

class UserDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityUserDetailBinding

    var viewModel: UserDetailViewModel? = null
    var repositoriesAdapter: RepositoriesAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var reposiArray: ArrayList<Repositories>? = null

    var login: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        login = intent.getStringExtra("login")


        binding.userProgres.visibility = View.VISIBLE
        viewModel = ViewModelProvider(this).get(UserDetailViewModel::class.java)
        viewModel!!.refreshData(this,login!!);
        repositoriesAdapter = RepositoriesAdapter(this, arrayListOf(),true)
        linearLayoutManager = LinearLayoutManager(this)
        binding.userRcyc.layoutManager = linearLayoutManager
        binding.userRcyc.adapter = repositoriesAdapter

        observeliveData()
    }

    fun observeliveData() {
        viewModel!!.data.observe(this, androidx.lifecycle.Observer { data ->
            data.let {
                if (data != null) {
                    reposiArray = ArrayList<Repositories>()
                    for (item in data) {
                        reposiArray!!.add(item!!)
                    }
                    repositoriesAdapter?.add(reposiArray)
                }
            }
        })

        viewModel!!.dataUser.observe(this, androidx.lifecycle.Observer { datauser ->
            datauser.let {
                if (datauser != null) {
                    binding.email.text = datauser.email
                    binding.username.text = datauser.name
                    Glide.with(this).load(datauser.avatarUrl).dontAnimate().into(binding.img)
                }
            }
        })

        viewModel!!.dataUserError.observe(this, androidx.lifecycle.Observer { datausererror ->
            datausererror?.let {
                if (it) {
                    Toast.makeText(this,"Bilinmedik bir hata oluştu", Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel!!.dataError.observe(this, androidx.lifecycle.Observer { error ->
            error?.let {
                if (it) {
                    Toast.makeText(this,"Bilinmedik bir hata oluştu", Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel!!.dataLoading.observe(this, androidx.lifecycle.Observer { loading ->
            loading?.let {
                if (it) {
                    binding.userProgres.visibility = View.VISIBLE
                } else {
                    binding.userProgres.visibility = View.GONE
                }
            }
        })
    }
}