package com.vodafone.studycase.view

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vodafone.studycase.adapter.RepositoriesAdapter
import com.vodafone.studycase.databinding.ActivityRepositoriesBinding
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.viewmodel.RepositoriesViewModel
import kotlin.collections.ArrayList

class RepositoriesActivity : AppCompatActivity() {

    var viewModel: RepositoriesViewModel? = null
    lateinit var binding: ActivityRepositoriesBinding
    var repositoriesAdapter: RepositoriesAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var reposiArray: ArrayList<Repositories>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepositoriesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.repoProgres.visibility = View.VISIBLE
        viewModel = ViewModelProvider(this).get(RepositoriesViewModel::class.java)
        viewModel!!.refreshData(this);
        repositoriesAdapter = RepositoriesAdapter(this, arrayListOf(),false)
        linearLayoutManager = LinearLayoutManager(this)
        binding.repoRecyc.layoutManager = linearLayoutManager
        binding.repoRecyc.adapter = repositoriesAdapter

        observeliveData()
    }

    fun observeliveData() {
        viewModel!!.data.observe(this, androidx.lifecycle.Observer { data ->
            data.let {
                if (data != null) {
                    reposiArray = ArrayList<Repositories>()
                    for (item in data) {
                        reposiArray!!.add(item!!)
                    }
                    repositoriesAdapter?.add(reposiArray)
                }
            }
        })

        viewModel!!.dataError.observe(this, androidx.lifecycle.Observer { error ->
            error?.let {
                if (it) {
                   Toast.makeText(this,"Bilinmedik bir hata oluştu",Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel!!.dataLoading.observe(this, androidx.lifecycle.Observer { loading ->
            loading?.let {
                if (it) {
                    binding.repoProgres.visibility = View.VISIBLE
                } else {
                    binding.repoProgres.visibility = View.GONE
                }
            }
        })
    }
}