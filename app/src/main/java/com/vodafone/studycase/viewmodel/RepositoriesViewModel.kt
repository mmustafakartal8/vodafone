package com.vodafone.studycase.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vodafone.studycase.api.ApiClient
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.ınterface.RepositoriesInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoriesViewModel : ViewModel() {

    lateinit var repositoriesInterface: RepositoriesInterface
    val data = MutableLiveData<Array<Repositories?>>()
    val dataError = MutableLiveData<Boolean>()
    val dataLoading = MutableLiveData<Boolean>()

    fun refreshData(context: Context) {
        repositoriesInterface = ApiClient.getRetrofit(context).create(RepositoriesInterface::class.java)
        val response = repositoriesInterface.getRepositories()
        response!!.enqueue(object : Callback<Array<Repositories?>?> {
            override fun onResponse(call: Call<Array<Repositories?>?>, response: Response<Array<Repositories?>?>) {
                if (response.isSuccessful){
                    data.value = response.body()
                    dataError.value = false
                }else{
                    dataError.value = true
                }
                dataLoading.value = false
            }

            override fun onFailure(call: Call<Array<Repositories?>?>, t: Throwable) {
                dataLoading.value = false
                dataError.value = true
                dataLoading.value = false
                t.printStackTrace()
            }
        })

    }
}