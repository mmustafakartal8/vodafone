package com.vodafone.studycase.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vodafone.studycase.api.ApiClient
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.model.User
import com.vodafone.studycase.ınterface.RepositoriesInterface
import com.vodafone.studycase.ınterface.UserDetailInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class UserDetailViewModel : ViewModel() {


    lateinit var userDetailInterface: UserDetailInterface
    val data = MutableLiveData<Array<Repositories?>>()
    val dataError = MutableLiveData<Boolean>()
    val dataLoading = MutableLiveData<Boolean>()

    val dataUser = MutableLiveData<User>()
    val dataUserError = MutableLiveData<Boolean>()

    fun refreshData(context: Context, login: String) {
        repositoriesRequest(context, login)
        userRequest(context, login)
    }

    fun userRequest(context: Context, login: String) {
        userDetailInterface = ApiClient.getRetrofit(context).create(UserDetailInterface::class.java)
        val response = userDetailInterface.getUsers(login)
        response!!.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    dataUser.value = response.body()
                    dataUserError.value = false
                } else {
                    dataUserError.value = true
                }
                dataLoading.value = false
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                dataLoading.value = false
                dataUserError.value = true
                dataUser.value = null
                t.printStackTrace()
            }

        })
    }

    fun repositoriesRequest(context: Context, login: String) {
        userDetailInterface = ApiClient.getRetrofit(context).create(UserDetailInterface::class.java)
        val response = userDetailInterface.getRepositories(login)
        response!!.enqueue(object : Callback<Array<Repositories?>?> {
            override fun onResponse(
                call: Call<Array<Repositories?>?>,
                response: Response<Array<Repositories?>?>
            ) {
                if (response.isSuccessful) {
                    data.value = response.body()
                    dataError.value = false
                } else {
                    dataError.value = true
                }
                dataLoading.value = false
            }

            override fun onFailure(call: Call<Array<Repositories?>?>, t: Throwable) {
                dataLoading.value = false
                dataError.value = true
                data.value = null
                t.printStackTrace()
            }
        })
    }

}