package com.vodafone.studycase.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vodafone.studycase.api.ApiClient
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.model.RepositoriesDetail
import com.vodafone.studycase.model.User
import com.vodafone.studycase.ınterface.RepositoriesDetailInterface
import com.vodafone.studycase.ınterface.RepositoriesInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoriesDetailViewModel : ViewModel() {

    lateinit var repositoriesDetailInterface: RepositoriesDetailInterface
    val data = MutableLiveData<RepositoriesDetail>()
    val dataError = MutableLiveData<Boolean>()
    val dataLoading = MutableLiveData<Boolean>()

    fun refreshData(context: Context,login: String, name: String) {
        repositoriesDetailInterface = ApiClient.getRetrofit(context).create(RepositoriesDetailInterface::class.java)
        val response = repositoriesDetailInterface.getReposDetail(login,name)
        response!!.enqueue(object : Callback<RepositoriesDetail> {
            override fun onResponse(call: Call<RepositoriesDetail>, response: Response<RepositoriesDetail>) {
                Log.e("call",call.request().toString())
                Log.e("callss",response.body()?.name.toString())
                if (response.isSuccessful) {
                    data.value = response.body()
                    dataError.value = false
                } else {
                    dataError.value = true
                }
                dataLoading.value = false
            }

            override fun onFailure(call: Call<RepositoriesDetail>, t: Throwable) {
                dataLoading.value = false
                dataError.value = true
                data.value = null
                t.printStackTrace()
            }

        })

    }
}