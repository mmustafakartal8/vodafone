package com.vodafone.studycase.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vodafone.studycase.databinding.ItemRepositoriesBinding
import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.view.RepositoriesDetailActivity

class RepositoriesAdapter(
    val context: Context,
    val repositoriesList: ArrayList<Repositories>?,
    val userView: Boolean
) :
    RecyclerView.Adapter<RepositoriesAdapter.RepositoriesHolder>() {
    lateinit var bindind: ItemRepositoriesBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoriesHolder {
        bindind = ItemRepositoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RepositoriesHolder(bindind, context, userView)
    }

    override fun onBindViewHolder(holder: RepositoriesHolder, position: Int) {
        val repositories: Repositories = repositoriesList!![position]
        holder.bind(repositories)
    }

    override fun getItemCount(): Int {
        return repositoriesList?.size!!
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun add(repolist: ArrayList<Repositories>?) {
        repositoriesList?.addAll(repolist!!)
        notifyDataSetChanged()
    }

    class RepositoriesHolder(
        private val itemBinding: ItemRepositoriesBinding,
        val context: Context,
        val userView: Boolean
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(repositories: Repositories) {
            itemBinding.repoName.text = repositories.fullName
            itemBinding.userName.text = repositories.name
            Glide.with(context).load(repositories.owner?.avatarUrl).dontAnimate()
                .into(itemBinding.avatar)

            itemBinding.view.setOnClickListener(View.OnClickListener {
                if (!userView) {
                    val intent = Intent(context, RepositoriesDetailActivity::class.java)
                    intent.putExtra("repositories", repositories)
                    context.startActivity(intent)
                }
            })
        }
    }

}
