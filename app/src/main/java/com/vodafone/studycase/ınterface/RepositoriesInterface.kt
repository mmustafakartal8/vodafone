package com.vodafone.studycase.ınterface

import com.vodafone.studycase.model.Repositories
import retrofit2.Call
import retrofit2.http.GET

interface RepositoriesInterface {
    @GET("/repositories")
    fun getRepositories(): Call<Array<Repositories?>?>?
}