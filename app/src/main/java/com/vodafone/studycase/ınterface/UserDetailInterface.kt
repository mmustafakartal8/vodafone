package com.vodafone.studycase.ınterface

import com.vodafone.studycase.model.Repositories
import com.vodafone.studycase.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface UserDetailInterface {
    @GET("/users/{login}/repos")
    fun getRepositories(@Path("login") login: String?): Call<Array<Repositories?>?>?

    @GET("/users/{login}")
    fun getUsers(@Path("login") login: String?): Call<User>?
}