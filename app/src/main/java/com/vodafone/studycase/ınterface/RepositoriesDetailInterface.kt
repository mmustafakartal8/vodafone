package com.vodafone.studycase.ınterface

import com.vodafone.studycase.model.RepositoriesDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RepositoriesDetailInterface {
    @GET("/repos/{login}/{name}")
    fun getReposDetail(@Path("login") login: String?,@Path("name") name: String?): Call<RepositoriesDetail>?
}